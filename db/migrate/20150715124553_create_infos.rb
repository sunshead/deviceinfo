class CreateInfos < ActiveRecord::Migration
  def change
    create_table :infos do |t|
      t.string :name
      t.string :location
      t.string :note
      t.string :macs
      t.string :type

      t.timestamps null: false
    end
  end
end
