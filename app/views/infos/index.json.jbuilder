json.array!(@infos) do |info|
  json.extract! info, :id, :name, :location, :note, :macs, :mactype
  json.url info_url(info, format: :json)
end
